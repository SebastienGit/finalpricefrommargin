﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

using Wpm.Implement.ComponentEditor;
using Wpm.Implement.Manager;
using Wpm.Implement.Processor;
using Wpm.Schema.Kernel;

using Alma.NetKernel;
using Alma.UI.Dxf;
using Alma.BaseUI.Utils;
using Alma.NetWrappers2D;


using Actcut.CommonModel;
using Actcut.CommonModelHelper;
using Actcut.CommonModelManager;
using Actcut.ActcutModelHelper;
using Actcut.ResourceManager;

using Actcut.ActcutModelManager;
using Actcut.NestingManager;
using Actcut.ActcutModel;

using Actcut.QuoteModel;
using Actcut.QuoteModelManager;
using Actcut.ActcutModelManagerUI;
using Alma.BaseUI.ErrorMessage;

namespace AlmaCamUser1
{
    class AlmaCamUser1
    {
        static void Main(string[] args)
        {
            MessageBox.Show("Start");

            // Manual selection of the database...
            IModelsRepository modelsRepository = new ModelsRepository();
            IContext Context = null;
            IEntitySelector entitySelector = new EntitySelector();
            entitySelector.ShowPropertyBox = false;
            entitySelector.MultiSelect = false;
            entitySelector.Init(modelsRepository.Context, modelsRepository.Context.Kernel.CompositeEntityTypeList["MODEL"]);
            if (entitySelector.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine("Connection to database...");
                foreach (IEntity model in entitySelector.SelectedEntity)
                {
                    Context = modelsRepository.GetModelContext("BALI-Deliver2");
                }
                Console.WriteLine("Connected");
            }
            else
                return;

            //New set final price
            IEntity quoteEntity = Context.EntityManager.GetEntity(12, "_QUOTE_REQUEST"); //Command.WorkOnEntity;
            if (quoteEntity != null)
            {
                ITransaction transaction = quoteEntity.Context.CreateTransaction();
                IQuote quote = new Quote(transaction, quoteEntity);

                EditMarginQuoteForm editMarginQuoteForm = null;
                try
                {
                    editMarginQuoteForm = new EditMarginQuoteForm(Context, quote.QuoteEntity, quote.QuoteEntity.GetFieldValueAsDouble("_GLOBAL_MARGIN"));
                    if (editMarginQuoteForm.ShowDialog() == DialogResult.OK)
                    {
                        quote.SetFinalPrice(editMarginQuoteForm.finalPrice, editMarginQuoteForm.discount);
                        transaction.Save();
                    }
                    QuoteTools.EvalulateQuote(quoteEntity, false, false, QuoteEvaluationMode.Force);
                }
                finally { if (editMarginQuoteForm != null) editMarginQuoteForm.Dispose(); }
            }
            return;

            //... Or connection to database with its name:
            //IModelsRepository modelsRepository = new ModelsRepository();
            //Context = modelsRepository.GetModelContext("DatabaseName");


            ////About Part Preparation
            //DrafterModule drafter = new DrafterModule();
            //drafter.Init(Context.Model.Id32, 1);
            //drafter.OpenMachinablePart(CamObjectType.CamObject, 1);

            ////Do things here

            ////Save and release Drafter
            //drafter.SaveMachinablePart(true);
            //drafter.Release();
            //drafter.Free();


            ////About Nesting 
            //IEntity nestingEntity = Context.EntityManager.GetEntity(1, "_NESTING");
            //INestingEngine nestingEngine = new NestingEngine();
            //nestingEngine.Init(Context, nestingEntity);
            //INesting nesting = nestingEngine.AddNesting(nestingEntity);

            ////Do things here

            ////Save the modification
            //ITransaction transaction = Context.CreateTransaction();
            //bool result = nestingEngine.Save(transaction);
            //transaction.Save();

            MessageBox.Show("End");
        }

        private static void ComputeFinalPriceFromMargin(IEntity quoteEntity, double margin, out double finalPrice, out double discount)
        {
            double costPrice = quoteEntity.GetFieldValueAsDouble("_IN_COST");

            finalPrice = costPrice / (1 - margin);
            discount = 0;
        }
    }
}